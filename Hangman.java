import java.util.Scanner;

public class Hangman {
	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("WELCOME TO HANGMAN!" + "\n" + "Input your word!");
		String word = reader.nextLine();
		System.out.print("\033c");
		runGame(word);
	}
	
	
	public static int isLetterInWord(String word, char c) {
		boolean letterFound = false;
		int positionOfLetter = 0;
		for (int i = 0; i < 4; i = i+1) {
			if (toUpperCase(word.charAt(i)) == toUpperCase(c)) {
				letterFound = true;
				positionOfLetter = i;
			}
		}
		if (letterFound == true)
			return positionOfLetter;
		else
			return -1;
	}
	
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
	}
	
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
		char[] letters = new char[] {'_','_','_','_'};
		
		if (letter0 == true)
			letters[0] = toUpperCase(word.charAt(0));
		
		if (letter1 == true)
			letters[1] = toUpperCase(word.charAt(1));
		
		if (letter2 == true)
			letters[2] = toUpperCase(word.charAt(2));
		
		if (letter3 == true)
			letters[3] = toUpperCase(word.charAt(3));
		
		System.out.println ("HANGMAN: " + letters[0] + ' ' + letters[1] + ' ' + letters[2] + ' ' + letters[3]);
	}
	
	public static void runGame(String word) {
		Scanner reader = new Scanner(System.in);
		int nbrGuess = 0;
		int lives = 6;
		boolean win = false;
		boolean[] letterThere = new boolean[] {false,false,false,false};
		
		while (lives > 0 && win == false) {
    		char[] guesses = new char[20];
    		if (nbrGuess == 0) {
        		System.out.println("GAMES START!" + "\n" + "Input your first GUESS!");
    		}
    		else {
    		    System.out.println("\n" + "Input your " + (nbrGuess+1) + " GUESS!");
    		}
    		nbrGuess++;
			
    		char guess = reader.next(".").charAt(0);
    		int position = isLetterInWord(word, guess);
            
			if (position != -1) {
    			letterThere[position] = true;
			}
    		printWork(word,letterThere[0],letterThere[1],letterThere[2],letterThere[3]);
			
    		 // Dialogues
    		if (isLetterInWord(word, guess) == -1) {
    		    lives--;
    		    if (nbrGuess <= 4) {
    		        System.out.println("Aw, you got it wrong.. TRY AGAIN!" + "\n");
    		    }
    		    if (nbrGuess > 4) {
    		        System.out.println("It's just ONE WORD, it's not that hard!" + "\n");
    		    }
    		}
    		else {
    		    if (nbrGuess <= 4) {
    		        System.out.println("You're awesome!" + "\n");
    		    }
    		    if (nbrGuess > 4) {
    		        System.out.println("Took you long enough!" + "\n");
    		    }
    		}
    		
    		if (letterThere[0] == true && letterThere[1] == true && letterThere[2] == true && letterThere[3] == true) {
    		    win = true;
    		    System.out.println("CONGRATS! YOU WON!" + "\n" + "The word was: " + word + "!");
    		}
		}
		
		if (lives == 0) {
	        System.out.println("YOU LOSE!" + "\n" + "The word was: " + word + "!");
	    }
	}
}