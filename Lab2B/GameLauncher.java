import java.util.Scanner;

public class GameLauncher {
	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		boolean playing = true;
		
		//loop to ask if player wishes to play again
		while(playing) {
			boolean gameSelected = false;
			//loop for if the player selects an options outside the selected options
			while(!gameSelected) {
				System.out.print("Let's play some games! \n Type the number associated to the game you want to play! \n 1: Wordle \n 2: Hangman (2 playes required) \n");
				int select = reader.nextInt();
				
				// if select 1, generates a word and plays Wordle
				if (select == 1) {
					String answer = Wordle.generateWord();
					Wordle.runGame(answer);
					gameSelected = true;
				}
				
				// if selects 2, asks for an input word and runs the Hangman game
				if (select == 2) {
					System.out.println("WELCOME TO HANGMAN!" + "\n" + "Input your 4 letter word!");
					String word = reader.next();
					System.out.print("\033c");
					Hangman.runGame(word);
					gameSelected = true;
				}
				else {
					System.out.println("Game not selected! \nTry again!");
				}
			}
			
			//Ask if user wishes to play again
			System.out.println("Want to play again? 1:YES 2:NO ");
			int choice = reader.nextInt();
			if (choice == 2) {
				playing = false;
			}
		}
	}
}